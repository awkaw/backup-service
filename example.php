<?php

require __DIR__ . "/vendor/autoload.php";

$logger = new \BackupService\Logger();

$backup = new \BackupService\BackupService($logger);

/* FTP */
$ftpProvider = new \BackupService\Providers\FtpProvider();
/* OR */
/* SFTP */
$sftpProvider = new \BackupService\Providers\SftpProvider();

$ftpProvider->setHost("ftp.backup.net");
$ftpProvider->setPort(21);
$ftpProvider->setUsername("ftp_user");
$ftpProvider->setPassword("ftp_password");

/* MYSQL */
$mysqlProvider = new \BackupService\Providers\MysqlProvider();

$mysqlProvider->setHost("localhost");
$mysqlProvider->setPort(3306);
$mysqlProvider->setUsername("root");
$mysqlProvider->setPassword("root");

/* TASK FILES */

$tasks = [
    "/var/www/site.ru/./" => "/backup/site.ru/",
    "/var/www/site2.ru/./" => "/backup/site2.ru/",
];

foreach ($tasks as $local => $remote) {

    $task = new \BackupService\Task("Backup dir {$local}");

    $task->setType(\BackupService\Task::TASK_TYPE_FILES_SYNC);
    $task->setTransportProvider($ftpProvider);
    $task->setLocalPath($local);
    $task->setServerPath($remote);

    $backup->run($task);
}

/* TASK DATABASES */

$databases = $mysqlProvider->getListDatabasesWithoutTechnical();

if(!empty($databases)){

    foreach ($databases as $database) {

        $task = new \BackupService\Task("Backup MySQL DB {$database}");

        $task->setType(\BackupService\Task::TASK_TYPE_MYSQL);
        $task->setTransportProvider($ftpProvider);
        $task->setMysqlProvider($mysqlProvider);
        $task->setLocalPath("/var/tmp/mysql/");
        $task->setServerPath("/backup/databases/");
        $task->setDatabaseName($database);

        $backup->run($task);
    }
}