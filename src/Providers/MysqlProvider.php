<?php

namespace BackupService\Providers;

use BackupService\Interfaces\DatabaseProviderInterface;
use BackupService\Interfaces\LoggerInterface;
use BackupService\Interfaces\Providerlnterface;

class MysqlProvider extends BaseDatabaseProvider implements Providerlnterface, DatabaseProviderInterface
{
    protected $socket = "/var/run/mysqld/mysqld.sock";

    private function getSecureFilePath(){
        return $this->getLocalPathForBackup()."mysql.sqlpwd";
    }

    public function createSecureFile(){

        $file = $this->getSecureFilePath();
        $dir = dirname($file);

        if(!file_exists($dir)){
            mkdir($dir, 0777, true);
        }

        //socket={$this->socket}\n

        file_put_contents($file, "[mysqldump]\nhost={$this->host}\nport={$this->port}\nuser={$this->username}\npassword={$this->password}\n[mysql]\nhost={$this->host}\nport={$this->port}\nuser={$this->username}\npassword={$this->password}");

        $this->execCommand("chmod 0600 {$file} && chown root:nogroup {$file}");
    }

    public function removeSecureFile(){

        $file = $this->getSecureFilePath();

        if(file_exists($file)){
            @unlink($file);
        }
    }

    public function getBackupFileName(): string
    {
        if(is_null($this->backupFileName) || strlen($this->backupFileName) < 1){
            return $this->getDatabaseName().".sql";
        }

        return $this->backupFileName;
    }

    public function backup(): bool
    {

        $backupPath = $this->getLocalPathForBackup().$this->getBackupFileName();

        if(!file_exists($this->getLocalPathForBackup()) && !$this->makeDir($this->getLocalPathForBackup())){
            return false;
        }

        $this->createSecureFile();

        $command = "ionice -c 3 mysqldump --defaults-file={$this->getSecureFilePath()} --single-transaction --quick --lock-tables=false {$this->getDatabaseName()} > {$backupPath}";

        $output = $this->execCommand($command);

        if($this->logger instanceof LoggerInterface){
            $this->logger->debug($output);
        }

        $this->removeSecureFile();

        return (file_exists($backupPath));
    }

    public function getListDatabases(): array {

        $result = [];

        $this->createSecureFile();

        $command = "mysql --defaults-file={$this->getSecureFilePath()} <<MY_QUERY\nSHOW DATABASES;\nMY_QUERY";

        $output = $this->execCommand($command);

        $this->removeSecureFile();

        if(!empty($output)){

            foreach ($output as $item) {

                if($item != "Database"){
                    $result[] = $item;
                }
            }
        }

        return $result;
    }

    public function getListDatabasesWithoutTechnical(): array {

        $exclude = ["mysql","performance_schema","sys","information_schema"];

        $databases = $this->getListDatabases();

        if(!empty($databases)){

            foreach ($databases as $key => $database) {

                if(in_array($database, $exclude)){
                    unset($databases[$key]);
                }
            }
        }

        return $databases;
    }
}