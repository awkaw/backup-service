<?php

namespace BackupService\Providers;

use BackupService\Interfaces\Providerlnterface;
use BackupService\Interfaces\TransportProviderInterface;

class FtpProvider extends BaseProvider implements Providerlnterface,TransportProviderInterface
{
    protected $maxConnections = 8;
    protected $removeNotExistsFiles = false;
    protected $symbolicLinksAsFiles = false;

    public function setMaxConnections(int $maxConnections){
        $this->maxConnections = $maxConnections;
    }

    public function setSymbolicLinksAsFiles(bool $asFiles): void
    {
        $this->symbolicLinksAsFiles = $asFiles;
    }

    public function setRemoveNotExistsFiles(bool $remove): void
    {
        $this->removeNotExistsFiles = $remove;
    }

    private function lftpCommand($command){

        $command = "set net:connection-limit {$this->maxConnections}; {$command}";

        $command = "lftp -e '{$command}' -p {$this->port} -u {$this->username},{$this->password} {$this->host}";

        return $this->execCommand($command);
    }

    public function sync(string $localPath, string $serverPath): bool
    {
        $exclude = $this->getExclude();

        $remove = ($this->removeNotExistsFiles)?"--delete":"";
        $asSymbolic = ($this->symbolicLinksAsFiles)?"L":"";

        $command = "mirror -R{$asSymbolic} {$remove} --exclude-glob ".implode(" --exclude-glob ", $exclude)." {$localPath} {$serverPath}; bye;";

        $this->lftpCommand($command);

        return true;
    }

    public function copy(string $localPath, string $serverPath): bool{

        $command = "mkdir -p {$serverPath}; cd {$serverPath}; put {$localPath}; bye;";

        $this->lftpCommand($command);

        return true;
    }

    public function fileExists(string $path): bool
    {
        $command = "ls ".dirname($path)."; bye;";

        $files = $this->lftpCommand($command);

        if(!empty($files)){

            $findFile = basename($path);

            foreach ($files as $file) {

                if(strpos($file, $findFile) !== false){
                    return true;
                }
            }
        }

        return false;
    }

    public function moveRemote(string $oldPath, string $newPath): bool
    {

        $command = "mv {$oldPath} {$newPath}; bye;";

        $this->lftpCommand($command);

        return true;
    }

    /**
     * @param string $localPath
     * @param string $serverPath
     * @return bool
     * @deprecated deprecated since version 1.0.7, use method copy
     */
    public function copyFromLocalToServer(string $localPath, string $serverPath): bool
    {
        return $this->copy($localPath, $serverPath);
    }

    /**
     * @param string $localPath
     * @param string $serverPath
     * @return bool
     * @deprecated deprecated since version 1.0.7, use method sync
     */
    public function syncFromLocalToServer(string $localPath, string $serverPath): bool
    {
        return $this->sync($localPath, $serverPath);
    }
}