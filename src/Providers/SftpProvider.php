<?php

namespace BackupService\Providers;

use BackupService\Interfaces\Providerlnterface;
use BackupService\Interfaces\TransportProviderInterface;

class SftpProvider extends BaseProvider implements Providerlnterface,TransportProviderInterface
{
    protected $maxConnections = 8;
    protected $removeNotExistsFiles = false;
    protected $symbolicLinksAsFiles = false;

    public function setMaxConnections(int $maxConnections){
        $this->maxConnections = $maxConnections;
    }

    public function setSymbolicLinksAsFiles(bool $asFiles): void
    {
        $this->symbolicLinksAsFiles = $asFiles;
    }

    public function setRemoveNotExistsFiles(bool $remove): void
    {
        $this->removeNotExistsFiles = $remove;
    }

    public function sync(string $localPath, string $serverPath): bool
    {
        $exclude = $this->getExclude();

        $remove = ($this->removeNotExistsFiles)?"--delete":"";
        $asSymbolic = ($this->symbolicLinksAsFiles)?"L":"";

        $command = "rsync -azr{$asSymbolic} {$remove} --exclude ".implode(" --exclude ", $exclude)." --rsh=\"sshpass -p {$this->password} ssh -p {$this->port} -l {$this->username}\" --rsync-path=\"mkdir -p ".dirname($serverPath)." && rsync\" {$localPath} {$this->host}:{$serverPath}";

        $this->execCommand($command);

        return true;
    }

    public function copy(string $localPath, string $serverPath): bool{

        $command = "rsync -azr --rsh=\"sshpass -p {$this->password} ssh -p {$this->port} -l {$this->username}\" --rsync-path=\"mkdir -p ".dirname($serverPath)." && rsync\" {$localPath} {$this->host}:{$serverPath}";

        $this->execCommand($command);

        return true;
    }

    public function fileExists(string $path): bool
    {
        // TODO: Implement fileExistsInRemoteServer() method.
    }

    public function moveRemote(string $oldPath, string $newPath): bool
    {
        // TODO: Implement moveInRemoteServer() method.
    }

    /**
     * @param string $localPath
     * @param string $serverPath
     * @return bool
     * @deprecated deprecated since version 1.0.7, use method copy
     */
    public function copyFromLocalToServer(string $localPath, string $serverPath): bool
    {
        return $this->copy($localPath, $serverPath);
    }

    /**
     * @param string $localPath
     * @param string $serverPath
     * @return bool
     * @deprecated deprecated since version 1.0.7, use method sync
     */
    public function syncFromLocalToServer(string $localPath, string $serverPath): bool
    {
        return $this->sync($localPath, $serverPath);
    }
}