<?php


namespace BackupService\Providers;


use BackupService\Interfaces\DatabaseProviderInterface;

class BaseDatabaseProvider extends BaseProvider implements DatabaseProviderInterface
{
    protected $database;
    protected $backupFileName = "";
    protected $localPath = "/tmp/";

    public function setDatabaseName(string $name)
    {
        $this->database = $name;
    }

    public function getDatabaseName(): string
    {
        return $this->database;
    }

    public function backup(): bool
    {
        return false;
    }

    public function setLocalPathForBackup(string $localPath)
    {
        $this->localPath = $localPath;
    }

    public function getLocalPathForBackup(): string
    {
        return $this->localPath;
    }

    public function setBackupFileName(string $backupFileName)
    {
        $this->backupFileName = $backupFileName;
    }

    public function getBackupFileName(): string
    {
        return $this->backupFileName;
    }
}