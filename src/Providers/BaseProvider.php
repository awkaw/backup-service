<?php


namespace BackupService\Providers;

use BackupService\Interfaces\LoggerInterface;

abstract class BaseProvider
{
    protected $host,$port,$socket,$username,$password;
    protected $logger;

    public function setHost(string $host): void
    {
        $this->host = $host;
    }

    public function setPort(int $port): void
    {
        $this->port = $port;
    }

    public function setSocket(string $socket): void
    {
        $this->socket = $socket;
    }

    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    protected function execCommand(string $command): array
    {

        exec($command, $output);

        return $output;
    }

    protected function makeDir(string $dir): bool
    {

        if(!file_exists($dir)){
            return mkdir($dir, 0755, true);
        }

        return true;
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    protected function getExclude()
    {
        return [
            "*.log",
            "*/logs/*",
            "*/sessions/*",
            "*/cache/*",
            "*/tmp/*",
            "*/temp/*",
        ];
    }
}