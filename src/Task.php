<?php

namespace BackupService;

use BackupService\Interfaces\DatabaseProviderInterface;
use BackupService\Interfaces\Providerlnterface;
use BackupService\Interfaces\TransportProviderInterface;

class Task{

    const TASK_TYPE_FILES_SYNC = "files_sync";
    const TASK_TYPE_MYSQL = "mysql";

    protected $name,$type,$transportProvider,$mysqlProvider,$localPath,$serverPath,$database;
    protected $backupFileName = "";
    protected $_removeTmpFile = true;

    public function __construct(string $name){
        $this->name = $name;
    }

    public function getName(): string{
        return $this->name;
    }

    public function setType(string $type){
        $this->type = $type;
    }

    public function getType(): string{
        return $this->type;
    }

    public function setTransportProvider(TransportProviderInterface $provider){
        $this->transportProvider = $provider;
    }

    public function getTransportProvider(): TransportProviderInterface{
        return $this->transportProvider;
    }

    public function setMysqlProvider(DatabaseProviderInterface $provider){
        $this->mysqlProvider = $provider;
    }

    public function getMysqlProvider(): DatabaseProviderInterface{
        return $this->mysqlProvider;
    }

    public function setLocalPath(string $localPath){
        $this->localPath = $localPath;
    }

    public function getLocalPath(): string{
        return $this->localPath;
    }

    public function setServerPath(string $serverPath){

        $serverPath = preg_replace('#\{date\}#', date("Y_m_d"), $serverPath);

        $this->serverPath = $serverPath;
    }

    public function getServerPath(): string{
        return $this->serverPath;
    }

    public function setDatabaseName(string $name){
        $this->database = $name;
    }

    public function getDatabaseName(): string{
        return $this->database;
    }

    public function setBackupFileName(string $name){
        $this->backupFileName = $name;
    }

    public function getBackupFileName(): string{
        return $this->backupFileName;
    }

    public function removeTmpFile(bool $remove): void{
        $this->_removeTmpFile = $remove;
    }

    public function isRemoveTmpFile(): bool{
        return $this->_removeTmpFile;
    }
}