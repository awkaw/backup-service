<?php

namespace BackupService\Interfaces;

interface Providerlnterface
{
    public function setHost(string $host): void;
    public function setPort(int $port): void;
    public function setSocket(string $socket): void;
    public function setUsername(string $username): void;
    public function setPassword(string $password): void;
    public function setLogger(LoggerInterface $logger): void;
}