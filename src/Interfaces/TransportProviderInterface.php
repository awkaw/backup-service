<?php


namespace BackupService\Interfaces;


interface TransportProviderInterface
{
    //public function makeDirToServer(string $dirServerPath): bool;
    //public function renameDirToServer(string $oldServerPath, string $newServerPath): bool;
    //public function copyFromServerToServer(string $sourceServerPath, string $targetServerPath): bool;

    /**
     * @param string $localPath
     * @param string $serverPath
     * @return bool
     * @deprecated deprecated since version 1.0.7, use method copy
     */
    public function copyFromLocalToServer(string $localPath, string $serverPath): bool;

    /**
     * @param string $localPath
     * @param string $serverPath
     * @return bool
     * @deprecated deprecated since version 1.0.7, use method sync
     */
    public function syncFromLocalToServer(string $localPath, string $serverPath): bool;

    public function copy(string $localPath, string $serverPath): bool;
    public function sync(string $localPath, string $serverPath): bool;
    public function setRemoveNotExistsFiles(bool $remove): void;
    public function setSymbolicLinksAsFiles(bool $asFiles): void;
    public function fileExists(string $path): bool;
    public function moveRemote(string $oldPath, string $newPath): bool;
}