<?php


namespace BackupService\Interfaces;


interface LoggerInterface
{
    public function debug($data);
    public function error($data);
}