<?php


namespace BackupService\Interfaces;


interface DatabaseProviderInterface
{
    public function setLocalPathForBackup(string $name);
    public function getLocalPathForBackup(): string;
    public function setDatabaseName(string $name);
    public function getDatabaseName(): string;
    public function setBackupFileName(string $name);
    public function getBackupFileName(): string;
    public function backup(): bool;
}