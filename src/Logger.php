<?php

namespace BackupService;

use BackupService\Interfaces\LoggerInterface;

class Logger implements LoggerInterface
{

    protected function log($data, $level = "debug"){

        $dir = __DIR__."/../logs/";

        if(function_exists("storage_path")){
            $dir = storage_path("logs");
        }

        if(!file_exists($dir)){
            mkdir($dir, 0755, true);
        }

        if(is_array($data) || is_object($data)){
            $data = print_r($data, true);
        }

        $logFile = "backup_{$level}.log";

        file_put_contents($dir.$logFile, "[".date("Y-m-d H:i:s")."] ".$data."\n", FILE_APPEND);
    }

    public function debug($data){
        $this->log($data, "debug");
    }

    public function error($data)
    {
        $this->log($data, "error");
    }
}