<?php

namespace BackupService;

use BackupService\Interfaces\LoggerInterface;
use BackupService\Interfaces\Providerlnterface;

error_reporting(E_ALL);
ini_set("display_errors", E_ALL);

class BackupService
{

    protected $task, $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function getLogger()
    {
        return $this->logger;
    }

    public function run(Task $task)
    {

        $this->task = $task;

        $this->logger->debug("Start task " . $task->getName());
        $start = microtime(true);

        switch ($task->getType()) {
            case Task::TASK_TYPE_FILES_SYNC:
                $task->getTransportProvider()->sync($this->task->getLocalPath(), $this->task->getServerPath());
            break;
            case Task::TASK_TYPE_MYSQL:

                $task->getMysqlProvider()->setLocalPathForBackup($this->task->getLocalPath());
                $task->getMysqlProvider()->setDatabaseName($this->task->getDatabaseName());
                $task->getMysqlProvider()->setBackupFileName($this->task->getBackupFileName());

                $localFilePath = $this->task->getLocalPath().$task->getMysqlProvider()->getBackupFileName();

                if ($task->getMysqlProvider()->backup()) {
                    $task->getTransportProvider()->copy($localFilePath, $this->task->getServerPath());
                }

                if($task->isRemoveTmpFile() && file_exists($localFilePath)){
                    unlink($localFilePath);
                }

            break;
        }

        $this->logger->debug("Success task ".$task->getName().", time: ".(microtime(true) - $start));
    }


    const DEBUG = true;

    const FTP = "ftp";
    const SFTP = "sftp";

    const REMOTE_PROTOCOL_1 = self::FTP;
    const REMOTE_HOST_1 = "";
    const REMOTE_PORT_1 = 21;
    const REMOTE_USER_1 = "";
    const REMOTE_PASSWORD_1 = "";

    const REMOTE_PROTOCOL_2 = self::FTP;
    const REMOTE_HOST_2 = "";
    const REMOTE_PORT_2 = 21;
    const REMOTE_USER_2 = "";
    const REMOTE_PASSWORD_2 = "";

    const REMOTE_SERVER_DEFAULT = 1;

    const REMOTE_DIR_DEFAULT = "/server";
    const REMOTE_DIR_SITES = "/server/sites";
    const REMOTE_DIR_DB = "/server/databases";

    const REMOTE_BACKUP_COUNT = 3;

    const MYSQL_USER = "root";
    const MYSQL_PASSWORD = "";
    const MYSQL_EXCLUDE_DB = [
        "performance_schema", "phpmyadmin", "information_schema", "mysql",
    ];

    const MYSQL_PERIOD_BACKUP = 43000;

    const DIRS = [];

    const TEMP_DIR = "/tmp/backups/"; // папка должна закрываться слешем "/"

    const FILE_MODIFIED_EXCLUDE_FILES = ["sitemap.xml"];
    const FILE_MODIFIED_EXCLUDE_EXT = ["log", "tmp", "cache"];
    const FILE_MODIFIED_EXCLUDE_DIRS = ["log", "logs", "tmp", "cache", "sessions"];

    const ZIP_EXCLUDE = ["log", "logs", "tmp", "cache"];

    public function log($data)
    {

        if (self::DEBUG) {

            if (is_array($data)) {
                print_r($data);
            } else {
                echo "{$data}";
            }

            echo "\n";
        }
    }

    private function getDateFromFtpFileName($fileName)
    {

        //date("Y_m_d_H_i_s")
        if (preg_match('#[\d]{4}_[\d]{2}_[\d]{2}_[\d]{2}_[\d]{2}_[\d]{2}#', $fileName, $matches)) {

            if (isset($matches[0])) {

                list($year, $month, $day, $hour, $minute, $second) = explode("_", $matches[0]);

                return "{$year}-{$month}-{$day} {$hour}:{$minute}:{$second}";
            }
        }

        return null;
    }

    public function start()
    {

        self::backupFiles();
        self::backupDatabases();
    }

    public function backupDatabases()
    {

        exec("mysql -h " . self::MYSQL_HOST . " -u " . self::MYSQL_USER . " -p" . self::MYSQL_PASSWORD . " -e 'show databases;'", $output);

        if (!empty($output)) {

            $DBs = [];

            foreach ($output as $item) {

                if (!in_array($item, self::MYSQL_EXCLUDE_DB) && $item != "Database") {
                    $DBs[] = $item;
                }
            }

            if (!empty($DBs)) {

                foreach ($DBs as $DB) {

                    $fileDB = "{$DB}." . date("Y_m_d_H_i_s") . ".sql";
                    $fullFileDBPath = self::TEMP_DIR . "{$fileDB}";

                    $processBackup = true;
                    $date = null;

                    /* Получаем список существующих бекапов для сравнения с текущими файлами */
                    $ftpListFiles = self::listFilesBackupServer(self::REMOTE_DIR_DB);

                    if (!empty($ftpListFiles)) {

                        foreach ($ftpListFiles as $ftpListFile) {

                            if ($processBackup && strpos($ftpListFile, "{$DB}.") > -1) {

                                $date = self::getDateFromFtpFileName($ftpListFile);

                                $processBackup = (is_null($date) || (strtotime("now") - strtotime($date)) >= self::MYSQL_PERIOD_BACKUP);
                            }
                        }
                    }

                    /*if($processBackup){
                        self::log("{$DB} require backup: last backup {$date}");
                    }else{
                        self::log("{$DB} is relevant backup: last backup {$date}");
                    }*/

                    if ($processBackup) {

                        self::log("Dumping {$DB} ...");

                        exec("mysqldump --add-drop-table --allow-keywords -q -c -h " . self::MYSQL_HOST . " -u " . self::MYSQL_USER . " -p" . self::MYSQL_PASSWORD . " {$DB} > {$fullFileDBPath}");

                        if (file_exists($fullFileDBPath)) {

                            $fullFileZipPath = str_replace('.sql', '.zip', $fullFileDBPath);

                            self::zip($fullFileDBPath, $fullFileZipPath);

                            if (file_exists($fullFileZipPath)) {

                                unlink($fullFileDBPath);

                                self::toBackupServer($fullFileZipPath, self::REMOTE_DIR_DB);

                                unlink($fullFileZipPath);
                            }
                        }

                        self::clearOldFilesInBackupServer($DB . ".", self::REMOTE_DIR_DB);
                    }
                }
            }
        }
    }

    public function backupFiles()
    {

        $backupDirs = [];

        foreach (self::DIRS as $rootDir => $options) {

            if (substr($rootDir, -1) == DIRECTORY_SEPARATOR) {
                $dirs = glob($rootDir . "*", GLOB_ONLYDIR);
            } else {
                $dirs = [$rootDir];
            }

            if (!empty($dirs)) {

                foreach ($dirs as $dir) {

                    $dirArray = explode(DIRECTORY_SEPARATOR, $dir);
                    end($dirArray);
                    $childDir = current($dirArray);

                    if (isset($options['exclude']) && in_array($childDir, $options['exclude'])) {
                        continue;
                    }

                    $remoteServer = (isset($options['remote_server'])) ? $options['remote_server'] : self::REMOTE_SERVER_DEFAULT;

                    $fileSize = self::fileSize($dir);
                    $lastModified = self::lastModified($dir);

                    $zipBaseName = "{$childDir}.modified.";
                    $zipName = $zipBaseName . date("Y_m_d_H_i_s", $lastModified) . ".zip";

                    $remoteDir = (isset($options['remote_dir'])) ? $options['remote_dir'] : self::REMOTE_DIR_DEFAULT;

                    /* Получаем список существующих бекапов для сравнения с текущими файлами */
                    $ftpListFiles = self::listFilesBackupServer($remoteDir, $remoteServer);

                    if (!in_array($zipName, $ftpListFiles)) {

                        $processBackup = true;
                        $date = null;

                        if (isset($options['period'])) {

                            if (!empty($ftpListFiles)) {

                                foreach ($ftpListFiles as $ftpListFile) {

                                    if ($processBackup && strpos($ftpListFile, "{$zipBaseName}") > -1) {

                                        $date = self::getDateFromFtpFileName($ftpListFile);

                                        $processBackup = (is_null($date) || (strtotime("now") - strtotime($date)) >= $options['period']);
                                    }
                                }
                            }
                        }

                        if ($processBackup) {
                            self::log("{$zipBaseName} require backup: last backup {$date}");
                        } else {
                            self::log("{$zipBaseName} is relevant backup: last backup {$date}");
                        }

                        if ($processBackup) {

                            $backupDirs[] = [
                                "zipName" => $zipName,
                                "zipBaseName" => $zipBaseName,
                                "dir" => $dir,
                                "fileSize" => $fileSize,
                                "lastModified" => $lastModified,
                                "remoteDir" => $remoteDir,
                                "backupCount" => (isset($options['backup_count'])) ? $options['backup_count'] : self::REMOTE_BACKUP_COUNT,
                                "remoteServer" => $remoteServer,
                            ];
                        }

                    }
                }
            }

            //print_r($backupDirs);
        }

        if (!file_exists(self::TEMP_DIR)) {
            mkdir(self::TEMP_DIR, 0755, true);
        }

        /* Чистим временную папку */
        exec("rm -R " . self::TEMP_DIR . "*");

        foreach ($backupDirs as $backupDir) {

            $fullArchivePath = self::TEMP_DIR . $backupDir['zipName'];

            self::zip($backupDir['dir'], $fullArchivePath);

            if (file_exists($fullArchivePath)) {

                self::toBackupServer($fullArchivePath, $backupDir['remoteDir'], $backupDir['remoteServer']);

                unlink($fullArchivePath);
            }

            self::clearOldFilesInBackupServer($backupDir['zipBaseName'], $backupDir['remoteDir'], $backupDir['backupCount'], $backupDir['remoteServer']);
        }
    }

    private function clearOldFilesInBackupServer($baseNameZip, $remoteDir, $backupCount = null, $remoteServer = null)
    {

        if (is_null($backupCount)) {
            $backupCount = self::REMOTE_BACKUP_COUNT;
        }

        if (is_null($remoteServer)) {
            $remoteServer = self::REMOTE_SERVER_DEFAULT;
        }

        /* Чистим старые бекапы */
        $ftpListFiles = self::listFilesBackupServer($remoteDir, $remoteServer);

        $backupFiles = [];

        if (!empty($ftpListFiles)) {

            foreach ($ftpListFiles as $ftpListFile) {

                if (strpos($ftpListFile, $baseNameZip) > -1) {
                    $backupFiles[] = $ftpListFile;
                }
            }

            if (!empty($backupFiles)) {

                rsort($backupFiles);

                $counter = 0;

                foreach ($backupFiles as $backupFile) {

                    if ($counter >= $backupCount) {
                        self::removeFileBackupServer($remoteDir . "/" . $backupFile, $remoteServer);
                    }

                    $counter++;
                }
            }
        }
    }

    private function zip($dir, $pathArchive)
    {

        self::log("Dir {$dir} zipping to {$pathArchive} ...");

        $zipCommand = "zip -ry {$pathArchive} {$dir}";

        if (!empty(self::ZIP_EXCLUDE)) {

            $zipCommand .= " -x ";

            foreach (self::ZIP_EXCLUDE as $item) {
                $zipCommand .= " \"*/\.{$item}\"";
            }
        }

        exec($zipCommand, $output);

        //self::log($zipCommand);
        //self::log($output);

        self::log("Dir {$dir} zipping success :)");
    }

    private function toBackupServer($file, $remoteDir = null, $remoteServer = null)
    {

        if (is_null($remoteDir)) {
            $remoteDir = self::REMOTE_DIR_DEFAULT;
        }

        if (is_null($remoteServer)) {
            $remoteServer = self::REMOTE_SERVER_DEFAULT;
        }

        if (self::REMOTE_PROTOCOL_1 == self::FTP) {
            self::toFtp($file, $remoteDir, $remoteServer);
        }

        if (self::REMOTE_PROTOCOL_1 == self::SFTP) {
            self::toSftp($file, $remoteDir, $remoteServer);
        }
    }

    private function toFtp($fullPathFile, $remoteDir = null, $remoteServer = null)
    {

        if (is_null($remoteDir)) {
            $remoteDir = self::REMOTE_DIR_DEFAULT;
        }

        if (is_null($remoteServer)) {
            $remoteServer = self::REMOTE_SERVER_DEFAULT;
        }

        $host = self::REMOTE_HOST_1;
        $port = self::REMOTE_PORT_1;
        $user = self::REMOTE_USER_1;
        $password = self::REMOTE_PASSWORD_1;

        if ($remoteServer == 2) {

            $host = self::REMOTE_HOST_2;
            $port = self::REMOTE_PORT_2;
            $user = self::REMOTE_USER_2;
            $password = self::REMOTE_PASSWORD_2;
        }

        $pathArray = explode(DIRECTORY_SEPARATOR, $fullPathFile);
        end($pathArray);
        $file = current($pathArray);

        self::log("File {$fullPathFile} transfer to FTP, dir {$remoteDir} ...");

        $command = "ftp -n -p {$host} {$port} <<SCRIPT";
        $command .= "\nquote USER {$user}";
        $command .= "\nquote PASS {$password}";

        if (strlen($remoteDir) > 1) {
            $command .= "\nmkd {$remoteDir}";
            $command .= "\ncd {$remoteDir}";
        }

        $command .= "\nlcd " . self::TEMP_DIR;
        $command .= "\nmput {$file}";
        $command .= "\nquit";
        $command .= "\nSCRIPT";

        exec($command, $output);

        //self::log($command);
        //self::log($output);

        self::log("File {$file} transfer success :)");
    }

    private function toSftp($fullPathFile, $remoteDir = null, $remoteServer = null)
    {

        /*if(is_null($remoteDir)){
            $remoteDir = self::REMOTE_DIR_DEFAULT;
        }

	    if(is_null($remoteServer)){
		    $remoteServer = self::REMOTE_SERVER_DEFAULT;
	    }*/
    }

    private function listFilesBackupServer($dir, $remoteServer = null)
    {

        if (is_null($remoteServer)) {
            $remoteServer = self::REMOTE_SERVER_DEFAULT;
        }

        $files = [];

        if (self::REMOTE_PROTOCOL_1 == self::FTP) {
            $files = self::listFilesFtp($dir, $remoteServer);
        }

        /*if(self::REMOTE_PROTOCOL_1 == self::SFTP){
            //$files = self::toSftp($file, $remoteDir);
        }*/

        return $files;
    }

    private function listFilesFtp($dir, $remoteServer = null)
    {

        if (is_null($remoteServer)) {
            $remoteServer = self::REMOTE_SERVER_DEFAULT;
        }

        $files = [];

        $host = self::REMOTE_HOST_1;
        $port = self::REMOTE_PORT_1;
        $user = self::REMOTE_USER_1;
        $password = self::REMOTE_PASSWORD_1;

        if ($remoteServer == 2) {

            $host = self::REMOTE_HOST_2;
            $port = self::REMOTE_PORT_2;
            $user = self::REMOTE_USER_2;
            $password = self::REMOTE_PASSWORD_2;
        }

        $command = "ftp -n -p {$host} {$port} <<SCRIPT";
        $command .= "\nquote USER {$user}";
        $command .= "\nquote PASS {$password}";

        $command .= "\nls {$dir}";
        $command .= "\nquit";
        $command .= "\nSCRIPT";

        //self::log($command);

        exec($command, $output);

        if (!empty($output)) {

            foreach ($output as $item) {

                $array = explode(" ", $item);
                end($array);
                $item = current($array);

                if (!in_array($item, [".", ".."])) {
                    $files[] = current($array);
                }
            }
        }

        return $files;
    }

    private function removeFileBackupServer($file, $remoteServer = null)
    {

        if (is_null($remoteServer)) {
            $remoteServer = self::REMOTE_SERVER_DEFAULT;
        }

        if (self::REMOTE_PROTOCOL_1 == self::FTP) {
            self::removeFileFtp($file, $remoteServer);
        }

        if (self::REMOTE_PROTOCOL_1 == self::SFTP) {
            self::removeFileSftp($file, $remoteServer);
        }
    }

    private function removeFileFtp($file, $remoteServer = null)
    {

        if (is_null($remoteServer)) {
            $remoteServer = self::REMOTE_SERVER_DEFAULT;
        }

        $host = self::REMOTE_HOST_1;
        $port = self::REMOTE_PORT_1;
        $user = self::REMOTE_USER_1;
        $password = self::REMOTE_PASSWORD_1;

        if ($remoteServer == 2) {

            $host = self::REMOTE_HOST_2;
            $port = self::REMOTE_PORT_2;
            $user = self::REMOTE_USER_2;
            $password = self::REMOTE_PASSWORD_2;
        }

        $command = "ftp -n -p {$host} {$port} <<SCRIPT";
        $command .= "\nquote USER {$user}";
        $command .= "\nquote PASS {$password}";

        $command .= "\ndele {$file}";
        $command .= "\nquit";
        $command .= "\nSCRIPT";

        exec($command, $output);

        self::log("Remove File {$file} server {$remoteServer}");
    }

    private function removeFileSftp($file, $remoteServer = null)
    {

        /*if(is_null($remoteServer)){
            $remoteServer = self::REMOTE_SERVER_DEFAULT;
        }

        $host = self::REMOTE_HOST_1;
        $user = self::REMOTE_USER_1;
        $password = self::REMOTE_PASSWORD_1;

        if($remoteServer == 2){

            $host = self::REMOTE_HOST_2;
            $user = self::REMOTE_USER_2;
            $password = self::REMOTE_PASSWORD_2;
        }*/

        self::log("Remove File {$file} server {$remoteServer}");
    }

    /* bytes */
    private function fileSize($dir)
    {

        $size = 0;

        if (file_exists($dir)) {

            $io = popen('/usr/bin/du -s -B1 ' . $dir, 'r');
            $size = fgets($io, 4096); //bytes
            $size = substr($size, 0, strpos($size, "\t"));
            pclose($io);
        }

        return $size;
    }

    private function lastModified($file, $fileTime = 0, $debugFiles = false)
    {

        if (!file_exists($file)) {
            return $fileTime;
        }

        if (is_dir($file)) {

            $items = glob($file . "/*");

            if (!empty($items)) {

                foreach ($items as $item) {

                    if (is_dir($item)) {

                        $explodeDir = explode(DIRECTORY_SEPARATOR, $item);
                        end($explodeDir);

                        if (in_array(current($explodeDir), self::FILE_MODIFIED_EXCLUDE_DIRS)) {
                            return $fileTime;
                        }
                    }

                    $fileTime = self::lastModified($item, $fileTime, $debugFiles);
                }
            }

        } else {

            $path_parts = pathinfo($file);

            if (isset($path_parts['extension']) && in_array($path_parts['extension'], self::FILE_MODIFIED_EXCLUDE_EXT)) {
                return $fileTime;
            }

            if (isset($path_parts['basename']) && in_array($path_parts['basename'], self::FILE_MODIFIED_EXCLUDE_FILES)) {
                return $fileTime;
            }

            $time = filemtime($file);

            if ($time > $fileTime) {

                $fileTime = $time;

                if ($debugFiles) {
                    self::log("{$file} {$time}");
                }
            }
        }

        return $fileTime;
    }
}